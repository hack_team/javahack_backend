# DotPoint backend

## REST API for DotPoint Android APP:

### Methods:
- `baseurl/api/rest/create_transaction?user_id: String, token` `CreateTransaction(val transaction_id: Long)`
- `baseurl/api/rest/get_transaction_status?transition_id: String` -> `GetTransactionStatus(val status: Int)`
- `baseurl` -> `GetTransactionIds(val statuses: Map<Int,String>)` - IN PGORGESS
- `baseurl/api/rest` -> `GetMethods(val methods: Array<String>)`

> Note: All methods need to auth-> user,passwd