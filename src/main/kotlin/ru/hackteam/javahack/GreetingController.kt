package ru.hackteam.javahack

import java.util.concurrent.atomic.AtomicLong
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController



import com.google.gson.Gson
import okhttp3.*
import org.slf4j.LoggerFactory
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import java.net.URL


@RestController
class GreetingController() {

    val counter = AtomicLong()


    @GetMapping("/api/rest")
    fun rest(): GetMethods {
        val methods = arrayOf("CreateTransaction(val transaction_id: Long)", "data class GetTransactionStatus(val status: Int)", "data class GetTransactionIds(val statuses: Map<Int,String>)")
        return GetMethods(methods)
    }

    @GetMapping("/api/rest/create_transaction")
    fun create_transaction(@RequestParam user_id: String, @RequestParam token: String): CreateTransaction {
        // TODO create token generation
        return CreateTransaction(1337)
    }

    @GetMapping("/api/rest/get_transaction_status")
    fun create_transaction(@RequestParam transition_id: String): GetTransactionStatus {
        // TODO create status generation
        return GetTransactionStatus(0)
    }

    @GetMapping("/api/rest/get_pay_history")
    fun get_pay_history(): GetPayHistory {
        var answers =  arrayOf(mapOf(
                "cost" to 250,
                "name" to "Капучино"
        ),
                mapOf(
                        "cost" to 300,
                        "name" to "Латте Маккиато"
                ),
                mapOf(
                        "cost" to 150,
                        "name" to "Руссиано"

                ),
                mapOf(
                        "cost" to 200,
                        "name" to "Espresso Kofee"

                ),
                mapOf(
                        "cost" to 400,
                        "name" to "Руссиано & Цезарь"

                ),
                mapOf(
                        "cost" to 300,
                        "name" to "Латте & Маффин"

                ),
                mapOf(
                        "cost" to 300,
                        "name" to "Арабика & Маффин"
                ),
                mapOf(
                        "cost" to 100,
                        "name" to "Маффин"
                ))
        return GetPayHistory(answers)
    }


}

