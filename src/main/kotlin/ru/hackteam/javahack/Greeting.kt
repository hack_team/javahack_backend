package ru.hackteam.javahack
import okhttp3.*
import java.util.*

data class CreateTransaction(val transaction_id: Long)
data class GetTransactionStatus(val status: Int)
data class GetTransactionIds(val statuses: Map<Int, String>)
data class GetMethods(val methods: Array<String>)
data class GetPayHistory(val history: Any)

