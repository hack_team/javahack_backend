package ru.hackteam.javahack

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class HtmlController {

  @GetMapping("/webpay")
  fun webpay(model: Model, @RequestParam cost: String): String {
    model["title"] = "Payment"
    model["money"] = "$cost"
    return "index"
  }

  @GetMapping("/")
  fun fire(): String {
    return "firebase"
  }
}

